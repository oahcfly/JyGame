﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    public class ThreadModule : BaseModule
    {
        public ThreadModule(string name = ModuleName.Thread) : base(name)
        {
        }

        public override void InitGame()
        {
            base.InitGame();
        }
    }
}
