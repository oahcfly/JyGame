﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    public class ResourceModule : BaseModule
    {
        public ResourceModule(string name = ModuleName.Res) : base(name)
        {

        }

        public override void InitGame()
        {
            base.InitGame();            

            ResourceManager.CreateIns();
            RefController.CreateIns();
        }
    }
}
