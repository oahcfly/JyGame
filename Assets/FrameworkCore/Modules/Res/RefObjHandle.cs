﻿using JyFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// 引用计数专用
/// </summary>
public class RefObjHandle : MonoBehaviour
{
    private string _resName;
    public string ResName { get { return _resName; } set { _resName = value; } }

    private void Awake()
    {
        // 克隆或者新实例化添加引用
        MessageManager.Ins.NotifyEvent(RefController.OnAddRef, _resName);
    }

    private void OnDestroy()
    {
        // 删除减少实例化
        MessageManager.Ins.NotifyEvent(RefController.OnReduceRef, _resName);
    }

}

