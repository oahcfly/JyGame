﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    public class ResourceManager
    {
        #region slington
        public static ResourceManager Ins { get { return _ins; } }
        public static void CreateIns()
        {
            if (_ins == null) _ins = new ResourceManager();
        }
        private static ResourceManager _ins;
        private ResourceManager() { Init(); }
        #endregion

        private AssetBundleManifest _jyGameManifest;
        private Dictionary<string, RefAssetBundle> _allAssetBundles;


        private void Init()
        {
            _allAssetBundles = new Dictionary<string, RefAssetBundle>();

            // 读取所有文件的Manifest
            if (_jyGameManifest == null)
            {
                // todo: 等待
                /*
                var ab = AssetBundle.LoadFromFile("JyGame");
                if (ab != null)
                {
                    _jyGameManifest = ab.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                    ab.Unload(false);
                }
                */
            }
        }

        public AssetBundleManifest JyGameManifest { get { return _jyGameManifest; } }

        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public T LoadRes<T>(string path) where T : UnityEngine.Object
        {
            return Resources.Load<T>(path);
        }


        public GameObject Instantiate(UnityEngine.Object obj)
        {
            GameObject tempObj = (GameObject)GameObject.Instantiate(obj);
            tempObj.name = obj.name;

            return tempObj;
        }

        public void LoadAssetBundle(string name)
        {
            AssetBundle.LoadFromFile(name);
        }

        /// <summary>
        /// 协程异步加载AssetBundle资源
        /// </summary>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        public IEnumerator LoadAssetBundleAsyn(YieldArgs yieldArgs)
        {
            RefAssetBundle refAb = null;
            if(_allAssetBundles.TryGetValue(yieldArgs.Path, out refAb))
            {
                yieldArgs.Obj = refAb.Ab;
                yield break;
            }

            string path = yieldArgs.Path;
            string[] dependencies = JyGameManifest.GetAllDependencies(path);
            int dependenciesLength = dependencies != null ? dependencies.Length : 0;
            if (dependenciesLength > 0)
            {
                for(int i = 0; i < dependenciesLength; i++)
                {
                    if (string.IsNullOrEmpty(dependencies[i])) continue;

                    YieldArgs ya = YieldArgs.GetTempYieldArgs(dependencies[i]);
                    yield return MonoHelper.Ins.StartCoroutine(LoadAssetBundleAsyn(ya));
                    YieldArgs.ReleaseYieldArgs(ya);                    
                }
            }

            AssetBundleCreateRequest abReq = AssetBundle.LoadFromFileAsync(path);
            while(!abReq.isDone)
            {
                yield return abReq;
            }

            if(abReq.assetBundle == null)
            {
                _allAssetBundles.Remove(path);
                Debug.LogError("资源没找到！" + path);
                yield break;
            }

            yieldArgs.IsDone = true;
            yieldArgs.Progress = 1;
            yieldArgs.Obj = abReq.assetBundle;

            refAb = new RefAssetBundle(yieldArgs.Path, abReq.assetBundle);
            _allAssetBundles.Add(path, refAb);
        }

    }
}
