﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public class YieldArgs : BasePoolObject
    {
        public string Path;
        public bool IsDone;
        public float Progress;
        public object Obj;

        /// <summary>
        /// 从对象池中取
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static YieldArgs GetTempYieldArgs(params object[] args)
        {
            return _yieldArgsPool.GetObject(args);
        }

        /// <summary>
        /// 放回对象池
        /// </summary>
        /// <param name="ya"></param>
        public static void ReleaseYieldArgs(YieldArgs ya)
        {
            _yieldArgsPool.ReleaseObject(ya);
        }

        public YieldArgs(params object[] args)
        {
            if (_yieldArgsPool == null) _yieldArgsPool = new ObjectPool<YieldArgs>();

            _args = args;
            Path = (string)args[0];
        }

        /// <summary>
        /// 初始化自己
        /// </summary>
        /// <param name="args"></param>
        public override void InitSelf(params object[] args)
        {
            _args = args;
            Path = (string)args[0];
        }

        /// <summary>
        /// 清理自己
        /// </summary>
        public override void CleanSelf()
        {
            Path = "";
            IsDone = false;
            Progress = 0;
            Obj = null;
            _args = null;
        }

        #region ObjectPool
        private static int YieldArgsCapacity = 20;
        private static ObjectPool<YieldArgs> _yieldArgsPool;
        #endregion
    }

}
