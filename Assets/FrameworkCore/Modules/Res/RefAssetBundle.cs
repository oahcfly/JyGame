﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    public class RefAssetBundle
    {
        public string Name;
        public int RefCount;
        public AssetBundle Ab;

        public RefAssetBundle()
        {
            AddRef();
        }

        public RefAssetBundle(string name, AssetBundle ab):this()
        {
            Ab = ab;
            Name = name;            
        }

        public void AddRef()
        {
            RefCount += 1;
        }

        public void DelRef()
        {
            RefCount -= 1;
            if(RefCount <= 0)
            {
                OnDestroy();
            }
        }        

        public void ClearSelf()
        {
            OnDestroy();
        }

        private void OnDestroy()
        {
            Ab.Unload(true);
        }
    }
}
