﻿using System.Net.Mime;
using UnityEngine;

namespace JyFramework
{
    public class PathHelper
    {
        // 资源文件夹
        public static string ResPath = Application.dataPath + "/ResourcesCore/";

        // 代码文件夹
        public static string CodePath = Application.dataPath + "/GameCore/";

        // 场景文件夹
        public static string ScenePath = ResPath + "/_scenes/";

        // todo: 移动端路径还需要添加

        // editor下，assetbundle模式下运行就是读取StreamingAssets文件夹内的资源

        // 非editor下，assetbundle模式下运行就是读取Application.persistentDataPath文件夹内的资源

        // 需要用到的路径：
        /*
         * 1. StreamingAssets
         * 2. Application.persistentDataPath
         * 3. 开发资源文件夹路径 ResPath
         * 4. 
         */


    }
}