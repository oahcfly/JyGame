﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameCore;
using UnityEngine;

namespace JyFramework
{
    public class ScriptModule : BaseModule
    {
        public ScriptModule(string name = ModuleName.Script) : base(name)
        {

        }

        public override void InitGame()
        {
            base.InitGame();

            // 添加MonoHelper辅助类
            GameApp.CurObj.AddComponent<MonoHelper>();

        }
    }
}
