﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    public class UIModule : BaseModule
    {
        public UIModule(string name = ModuleName.UI):base(name)
        {
            
        }

        public override void InitGame()
        {
            base.InitGame();

            // 初始化UIManager 并且显示第一个界面
            UIManager.CreateIns();
        }
    }
}
