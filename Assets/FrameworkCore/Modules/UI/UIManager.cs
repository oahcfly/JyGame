﻿namespace JyFramework
{
    public class UIManager
    {
        #region Singleton
        private UIManager() { Init(); }
        public static void CreateIns() { _ins = new UIManager(); }
        private static UIManager _ins;
        public static UIManager Ins { get {return _ins;} }
        #endregion

        private void Init()
        {
            
        }
    }
}