﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    public class ObjectModule : BaseModule
    {
        public ObjectModule(string name = ModuleName.Object) : base(name)
        {
        }

        public override void InitGame()
        {
            base.InitGame();            
        }
    }
}
