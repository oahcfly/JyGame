# Event事件模块
- 系统中的核心模块
    * _MessageManager_ 在 _Event_ 模块的初始化中初始化
    * 可以通过继承 _EventController_ 直接在  _EventController_  内部做事件注册监听等
    * 也可以直接通过 _MessageManager_ 做全局的事件注册监听