﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public class EventModule : BaseModule
    {
        public EventModule(string name = ModuleName.Event):base(name)
        {

        }

        public override void InitGame()
        {
            base.InitGame();
            MessageManager.CreateIns();
        }
    }
}
