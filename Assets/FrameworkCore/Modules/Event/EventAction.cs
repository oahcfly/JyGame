﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public delegate void MessageHandler(object[] args);

    public delegate void HandlerMsg(string name, MessageHandler msg);
}
