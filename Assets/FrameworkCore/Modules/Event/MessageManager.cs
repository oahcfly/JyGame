﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public class MessageManager : BaseManager
    {
        public static MessageManager Ins { get { return _ins; } }

        public static void CreateIns()
        {
            _ins = new MessageManager();
        }

        private static MessageManager _ins = null;


        public MessageManager() : base()
        {

        }

        public override void Init()
        {
            _eventCtrls = new List<EventController>();
            _allEvents = new Dictionary<string, List<MessageHandler>>();
            _eventCtrl = new EventController("MessageManagerEventController");
        }

        /// <summary>
        /// 移除Manager
        /// </summary>
        public override void Remove()
        {
            base.Remove();
            RemoveAllCtrl();
        }

        /// <summary>
        /// 添加事件控制器
        /// </summary>
        /// <param name="eventCtrl"></param>
        public void AddEventCtrl(EventController eventCtrl)
        {
            if (eventCtrl == null || _eventCtrls.Contains(eventCtrl)) return;
            _eventCtrls.Add(eventCtrl);
            eventCtrl.OnInit();

            eventCtrl.RegistMsgEvent = RegistEvent;
            eventCtrl.RemoveMsgEvent = RemoveEvent;
        }

        /// <summary>
        /// 移除控制器
        /// </summary>
        /// <param name="eventCtrl"></param>
        public void RemoveCtrl(EventController eventCtrl)
        {
            if (eventCtrl == null || !_eventCtrls.Contains(eventCtrl)) return;

            _eventCtrls.Remove(eventCtrl);
            eventCtrl.OnRemove();

            eventCtrl.RegistMsgEvent = null;
            eventCtrl.RemoveMsgEvent = null;
        }

        /// <summary>
        /// 移除所有的控制器
        /// </summary>
        public void RemoveAllCtrl()
        {
            if (_eventCtrls == null || _eventCtrls.Count <= 0) return;
            foreach (var ec in _eventCtrls)
            {
                ec.OnRemove();
            }
            _eventCtrls.Clear();
        }

        /// <summary>
        /// 注册事件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="msg"></param>
        public void RegistEvent(string name, MessageHandler msg)
        {
            if (msg == null || _allEvents == null) return;

            List<MessageHandler> events = null;
            if (_allEvents.TryGetValue(name, out events))
            {
                events.Add(msg);
            }
            else
            {
                events = new List<MessageHandler>();
                events.Add(msg);
                _allEvents.Add(name, events);
            }
        }

        /// <summary>
        /// 通知事件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        public void NotifyEvent(string name, params object[] args)
        {
            if (_allEvents == null) return;
            List<MessageHandler> events = null;
            if (_allEvents.TryGetValue(name, out events))
            {
                foreach (var e in events)
                {
                    e(args);
                }
            }
            else
            {
                // log: the event is null on the notify
            }
        }

        /// <summary>
        /// 移除事件
        /// </summary>
        /// <param name="name"></param>
        /// <param name="msg"></param>
        public void RemoveEvent(string name, MessageHandler msg)
        {
            if (msg == null || _allEvents == null || !_allEvents.ContainsKey(name)) return;

            List<MessageHandler> events = null;
            if (_allEvents.TryGetValue(name, out events))
            {
                events.Remove(msg);
                if (events.Count <= 0)
                {
                    _allEvents[name].Clear();
                    _allEvents.Remove(name);
                }
            }
        }

        protected EventController _eventCtrl;
        protected List<EventController> _eventCtrls;
        protected Dictionary<string, List<MessageHandler>> _allEvents;
    }
}
