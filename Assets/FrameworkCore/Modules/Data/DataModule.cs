﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 数据模块,包括：
    /// <1>数据库 <2>读表 <3>大多数特殊的数据 <4>其他数据
    /// </summary>
    public class DataModule : BaseModule
    {
        public DataModule(string name = ModuleName.Data) : base(name)
        {
            
        }

        public override void InitGame()
        {
            base.InitGame();
        }
    }
}
