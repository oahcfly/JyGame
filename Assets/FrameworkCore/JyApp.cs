﻿using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JyFramework
{
    public class JyApp : BaseApp
    {
        public JyApp(string name = "JyGame"):base(name)
        {
            Debug.Log("JyApp Ctor");
        }

        protected override void AddModules()
        {
            _modules.Add(ModuleName.Event,      new EventModule()       );
            _modules.Add(ModuleName.Data,       new DataModule()        );
            _modules.Add(ModuleName.Res,        new ResourceModule()    );
            _modules.Add(ModuleName.UI,         new UIModule()          );
            _modules.Add(ModuleName.Log,        new LogModule()         );
            _modules.Add(ModuleName.Time,       new TimeModule()        );
            _modules.Add(ModuleName.Util,       new UtilModule()        );
            _modules.Add(ModuleName.Aduio,      new AudioModule()       );
            _modules.Add(ModuleName.Object,     new ObjectModule()      );
            _modules.Add(ModuleName.Script,     new ScriptModule()      );
            _modules.Add(ModuleName.Thread,     new ThreadModule()      );
            _modules.Add(ModuleName.Network,    new NetworkModule()     );
            _modules.Add(ModuleName.AI,         new AIModule()          );
        }
    }
}