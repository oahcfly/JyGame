﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JyFramework
{

    public class BaseApp : IGameLife
    {
        /*
         * 游戏框架的核心抽象类
         * 包括了：资源加载、UI、数据、网络、对象池、脚本、声音、时间、线程、工具、这些模块
         */
        /// <summary>
        /// GameApp名字
        /// </summary>
        public string Name { get { return _name; } }

        /// <summary>
        /// todo:
        /// </summary>
        public Dictionary<string, BaseModule> Modules { get { return _modules; } }


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="name"> 游戏名 </param>
        protected BaseApp(string name = "BaseApp")
        {
            _name = name;
            _modules = new Dictionary<string, BaseModule>();
            AddModules();
            InitGame();
        }

        public virtual void InitGame()
        {
            foreach(var m in _modules.Values)
            {
                m.InitGame();
            }
        }

        public virtual void StartGame()
        {
            foreach (var m in _modules.Values)
            {
                m.StartGame();
            }
        }

        public virtual void UpdateGame(float deltaTime)
        {
            foreach (var m in _modules.Values)
            {
                m.UpdateGame(deltaTime);
            }
        }

        public virtual void PauseGame()
        {
            foreach (var m in _modules.Values)
            {
                m.PauseGame();
            }
        }

        public virtual void ExitGame()
        {
            foreach (var m in _modules.Values)
            {
                m.ExitGame();
            }
        }

        /// <summary>
        /// 基本用不到,高层一般用Manager内的就行了
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetModule<T>(string name) where T : BaseModule
        {
            if (_modules.ContainsKey(name))
                return (T)_modules[name];

            return null;
        }

        /// <summary>
        /// 添加Moduel
        /// </summary>
        /// <param name="module"></param>
        public void AddModule(BaseModule module)
        {
            if (module == null) return;
            if (_modules.ContainsKey(module.Name)) return;

            _modules.Add(module.Name, module);
        }

        /// <summary>
        /// 移除Module
        /// </summary>
        public void RemoveModule(BaseModule module)
        {
            if (module == null) return;
            if (!_modules.ContainsKey(module.Name))
            {
                module.ClearSelf();
                return;
            }
            _modules.Remove(module.Name);
            module.ClearSelf();
        }
    
        protected string _name;
        protected Dictionary<string, BaseModule> _modules;

        protected virtual void AddModules()
        {

        }
    }
}