﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public class BaseManager
    {
        public virtual void Init()
        {

        }

        public virtual void Remove()
        {

        }

    }
}
