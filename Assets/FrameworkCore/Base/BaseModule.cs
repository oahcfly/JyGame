﻿using JyFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 游戏基础模块
    /// </summary>
    public class BaseModule : IGameLife, IModule
    {
        /// <summary>
        /// 当前模块名
        /// </summary>
        public string Name { get { return _name; } }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="ec"> 外部传入的eventctrl </param>
        protected BaseModule(string name = "BaseModule")
        {
            _name = name;
            _mgrs = new List<BaseManager>();
            _ctrls = new List<BaseController>();
            Debug.Log(name + ".ctor();");
        }

        public virtual void InitGame()
        {
            Debug.Log(_name + ".InitGame();");
        }

        public virtual void StartGame()
        {
            Debug.Log(_name + ".StartGame();");
        }

        public virtual void UpdateGame(float deltaTime)
        {
        }

        public virtual void PauseGame()
        {
            Debug.Log(_name + ".PauseGame();");
        }

        public virtual void ExitGame()
        {
            Debug.Log(_name + ".ExitGame();");
        }

        public virtual void AddManager(BaseManager mgr)
        {
            if (mgr == null || _mgrs.Contains(mgr)) return;
            _mgrs.Add(mgr);
            mgr.Init();
        }

        public virtual void RemoveManager(BaseManager mgr)
        {
            if (mgr == null) return;
            if (!_mgrs.Contains(mgr))
            {
                mgr.Remove();
                return;
            }

            _mgrs.Remove(mgr);
            mgr.Remove();
        }

        public virtual void AddController(BaseController ctrl)
        {
            if (ctrl == null || _ctrls.Contains(ctrl)) return;
            _ctrls.Add(ctrl);
            ctrl.Init();
        }

        public virtual void RemoveController(BaseController ctrl)
        {
            if (ctrl == null) return;
            if(!_ctrls.Contains(ctrl))
            {
                ctrl.Remove();
                return;
            }
            _ctrls.Remove(ctrl);
            ctrl.Remove();
        }

        public virtual void ClearSelf()
        {
            Debug.Log(_name + ".ClearSelf();");
            _mgrs.Clear();
            _ctrls.Clear();
            _mgrs = null;
            _ctrls = null;
        }

        protected string _name;
        protected List<BaseManager> _mgrs;
        protected List<BaseController> _ctrls;
    }

}