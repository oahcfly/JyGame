﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public class JyGameConfig
    {
        // 游戏名字
        public const string GameName = "JyGame";

        // 游戏版本<介绍：<1>1.<2>1.<3>1.<4>0, 其中4是小bug，3是小bug累积到10后自动加1， 2是一般更新， 1是大版本更新>
        public static string GameVersion = "1.0.0.0";


    }
}
