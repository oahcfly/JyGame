﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    /// <summary>
    /// 模块的接口
    /// </summary>
    public interface IModule
    {
        void AddManager(BaseManager mgr);
        void AddController(BaseController ctrl);
        void RemoveManager(BaseManager mgr);
        void RemoveController(BaseController ctrl);
        void ClearSelf();
    }
}
