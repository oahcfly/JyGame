﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    public interface IEvent
    {
        string Name { get; }
        void RegistEvent(string name, MessageHandler eventActioin);
        void NotifyEvent(string name, params object[] args);
        void RemoveEvent(string name, MessageHandler eventAction);
        Dictionary<string, List<MessageHandler>> Events { get; }
    }
}
