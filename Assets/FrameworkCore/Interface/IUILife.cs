﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.FrameworkCore.Interface
{
    /// <summary>
    /// UI的生命周期
    /// </summary>
    public interface IUILife
    {
        /// <summary>
        /// 初始化UI
        /// </summary>
        void Init();
        /// <summary>
        /// 显示UI
        /// </summary>
        void Show();
        /// <summary>
        /// 每帧更新
        /// </summary>
        void Update();
        /// <summary>
        /// 隐藏UI
        /// </summary>
        void Hide();
        /// <summary>
        /// 从内存和管理中移除UI
        /// </summary>
        void Remove();
    }
}
