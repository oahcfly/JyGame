﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyFramework
{
    /// <summary>
    /// 游戏生命周期
    /// </summary>
    public interface IGameLife
    {
        /// <summary>
        /// 准备框架、资源和数据
        /// </summary>
        void InitGame();
        /// <summary>
        /// 开始游戏
        /// </summary>
        void StartGame();
        /// <summary>
        /// 每帧更新
        /// </summary>
        /// <param name="deltaTime"></param>
        void UpdateGame(float deltaTime);
        /// <summary>
        /// 暂停游戏
        /// </summary>
        void PauseGame();
        /// <summary>
        /// 离开游戏
        /// </summary>
        void ExitGame();
    }
}
