﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JyFramework;

namespace GameCore
{
    public class GameApp : MonoBehaviour
    {
        public static GameApp Ins { get { return _ins; } }
        public static JyApp App { get { return Ins._app; } }
        public static GameObject CurObj { get { return Ins.gameObject; } }

        private static GameApp _ins;

        private JyApp _app;

        void Awake()
        {
            _ins = GetComponent<GameApp>();

            _app = new JyApp("JyGame");
            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {
            _app.StartGame();
        }

        void Update()
        {
            _app.UpdateGame(Time.deltaTime);
        }

        void OnDestroy()
        {
            _app.ExitGame();
        }
    }
}